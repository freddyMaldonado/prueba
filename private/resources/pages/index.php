<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 01:03 PM
 */
$template = new template("main");
$lstProducts = [
    ["id" => "1","name" => "Chevrolet Beat"     ,"price" => "380", "currency_code"=> "MXN", "type" => "ECMR " , "category" => "Auto Economico"],
    ["id" => "2","name" => "Chevrolet Spark"    ,"price" => "380", "currency_code"=> "MXN", "type" => "ECMR " , "category" => "Auto Economico"],
    ["id" => "3","name" => "Nissan March"       ,"price" => "380", "currency_code"=> "MXN", "type" => "ECMR " , "category" => "Auto Economico"],
    ["id" => "4","name" => "VW Gol Sedan"       ,"price" => "520", "currency_code"=> "MXN", "type" => "ICMR " , "category" => "Auto Intermedio"],
    ["id" => "5","name" => "Chevrolet Aveo"     ,"price" => "520", "currency_code"=> "MXN", "type" => "ICMR " , "category" => "Auto Intermedio"],
    ["id" => "6","name" => "Chevrolet Sonic"    ,"price" => "520", "currency_code"=> "MXN", "type" => "ICMR"  , "category" => "Auto Intermedio"],
];
?>
    <section id="myProducts">
        <div class="container">
            <h1 class="text-center">Autos disponibles para ti</h1>
            <h5 class="text-muted text-center">Encuentra el auto perfecto al mejor precio del mercado</h5>

            <div class="carouselProducts">
                <?php
                if(is_array($lstProducts) && count($lstProducts)>0){
                    foreach ($lstProducts as $_index => $actual){
                        ?>
                        <div class="container">
                            <div class="card card-product" align="center">
                                <img class="card-img-top img-cart" src="<?php echo asset("images/carts/id_".$actual["id"].".jpg")?>" alt="<?php echo $actual["name"]?>">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $actual["name"]?> / <small class="text-muted"><?php echo $actual["type"]?> </small></h5>
                                    <h6 class="text-muted"><?php echo $actual["category"]?> </h6>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <p class="text-muted text-center">  <i class="fas fa-snowflake"></i> A/C</p>
                                                <p class="text-muted text-center"><i class="fas fa-cog"></i>Manual</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 text-left">
                                                <p class="text-muted text-center"> <i class="fas fa-suitcase"></i> x 2</p>
                                                <p class="text-muted text-center"><i class="fas fa-user"></i></i> x2</p>
                                            </div>
                                        </div>
                                        <h2 class="text-center">$<?php echo $actual["price"]?><small>/<?php echo $actual["currency_code"]?></small></h2>
                                    </div>
                                    <button class="btn btn-primary btn-lg btn-block add-to-cart" data-info='<?php echo json_encode($actual)?>'><i class="fas fa-cart-arrow-down"></i> Agregar al carrito</button>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <script>
            $(function () {
                $(".carouselProducts").slick({
                    infinite: true,
                    arrows:true,
                    dots:true,
                    slidesToShow: 3,
                    autoplay: false,
                    autoplaySpeed: 2000,
                    slidesToScroll: 3,
                    focusOnSelect: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                infinite: true,
                                dots: true
                            }
                        },
                        {
                            breakpoint: 770,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
                $(".add-to-cart").on("click",function () {
                    var info     = $(this).data("info");
                    var request  = {
                        url : "add_to_cart",
                        data : info,
                        method: "POST"
                    };
                    main_async_request(request).then(
                        function (resp) {
                            var classe = "danger";
                            if(resp['success'] ==="1"){
                                classe = "success";
                                var qty_items = resp['qty'];
                                update_qty_cart(qty_items);
                            }
                            show_notification(resp['msg'],classe);
                        }
                    )

                });
            });



        </script>
    </section>
<?php
$arrConf = [
    "title" => "America Car Rental | renta-de-autos",
    "description" => "renta-de-autos"
];
$template->render($arrConf);//rederizamos la pagina
