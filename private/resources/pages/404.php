<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 06:31 PM
 */

$template = new template("main");
?>
    <div class="container-fluid text-center">
        <img src="<?php echo asset("images/404.png")?>">
    </div>
<?php
$arrayConf = [
    "title" => "404",
    "description" => "Error 404"
];
$template->render($arrayConf);