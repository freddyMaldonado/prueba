<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $description?>">
    <meta name="author" content="">
    <title><?php echo $title?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo  asset("assets/bootstrap/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css">
    <!-- Fontawesome core CSS -->
    <link href="<?php echo  asset("assets/fontawesome/css/all.min.css")?>" rel="stylesheet" type="text/css">
    <!-- Slick Carousel core CSS -->
    <link href="<?php echo  asset("assets/slick-carousel/css/slick.min.css")?>" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo  asset("css/styles.css")?>" rel="stylesheet"  type="text/css">
    <!-- Juery JS -->
    <script type="text/javascript" src="<?php echo  asset("assets/jquery/js/jquery.min.js")?>"></script>

</head>
<body>
<?php
$arrMenu = ["Inicio", "Vehiculos","Ofertas","Sucursales","Politicas", "Contacto"];
?>
<header>
    <nav class="navbar navbar-expand-md navbar-main navbar-dark">
        <a class="navbar-brand" href="<?php echo  route()?>"> <img src="<?php echo  asset("images/logo.png")?>"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <?php
                if(is_array($arrMenu) && count($arrMenu)>0){
                    foreach ($arrMenu as $i => $actual){
                        $classActive = ($i ==0)?"active":"";
                        ?>
                        <li class="nav-item <?php echo  $classActive?>" style="display: inline-block">
                            <a class="nav-link" href="<?php echo  route()?>"><?php echo  $actual?><span class="sr-only">(current)</span></a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
            <?php
            $qty    = (isset($_SESSION["cart"]) && is_array($_SESSION["cart"]) &&  count($_SESSION["cart"])>0)?count($_SESSION["cart"]):"0"
            ?>
            <div class="navbar-nav cart-icon">
                <a title="Ver los servicios del carrito" class="nav-link" id="cart-list">
                    <i class="fas fa-shopping-cart"></i>
                    <span class="badge badge-light" id="qty-items"><?php echo  $qty?></span>
                </a>
            </div>
        </div>
    </nav>
</header>
<?php
include (dirname(__FILE__,2)."/includes/modal.php");// incluimos el modal
?>
<!-- /.Contenedor Principal -->
<div class="container">
    <div class="row">
        <div class="notifications" id="notifications"></div>
    </div>
</div>
<main role="main">
    <?php echo  $content?>
</main>
<a href="#" id="toTopBtn" class="cd-top text-replace js-cd-top cd-top--is-visible cd-top--fade-out" ></a>
<div class="container">
    <footer class="">
        <p><a href="https://americacarrental.com.mx/" target="_blank">&copy; <?php echo  date("Y")?> America Car Rental </a></p>
    </footer>
</div>
<!-- Bootstrap core JS -->
<script type="text/javascript" src="<?php echo  asset("assets/bootstrap/js/bootstrap.min.js")?>"></script>
<!-- Slick Carousel core JS -->
<script type="text/javascript" src="<?php echo  asset("assets/slick-carousel/js/slick.min.js")?>" ></script>
<!-- Custom JS -->
<script type="text/javascript" src="<?php echo  asset("js/scripts.js")?>"></script>
<script>
    $(function(){
        $("#cart-list").on("click",function () {
            get_cart_list();
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 20) {
                $('#toTopBtn').fadeIn();
            } else {
                $('#toTopBtn').fadeOut();
            }
        });

        $('#toTopBtn').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 1000);
            return false;
        });
    })
</script>


</body>
</html>