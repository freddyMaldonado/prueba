<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 01:43 PM
 */
session_start();
//---------------------------------
// HELPERS
//---------------------------------
function validador($validar,$inputs){
    $errores = array();
    $valida_tipo = function ($tipo,$val){
        $flag = false;
        switch ($tipo){
            case "number":
                if(is_numeric($val) && $val>0){
                    $flag = true;
                }
                break;
            case "text":
                if(strlen($val)>0){
                    $flag = true;
                }
                break;
        }
        return $flag;
    };
    if(is_array($validar) && count($validar)>0){
        foreach ($validar as $field => $conf){
            $flag = false;
            $valor = $inputs[$field];
            if($conf["required"] == "1" ){
                if($valor != "" && $valor != null){
                    if($valida_tipo($conf["type"],$valor)){
                        $flag = true;
                    }
                }
            }elseif($valor !=""){
                if($valida_tipo($conf["type"],$valor)){
                    $flag = true;
                }
            }

            if($flag === false){
                $errores[] = array("field" => $field, "msg"=> "El campo: ".$conf["etiqueta"]." es incorrecto") ;
            }

        }
    }
    $success = (is_array($errores) && count($errores)>0)?"0": "1";
    return array("success" => $success, "errores" => $errores);
}
function asset($path=""){
    return $path;
}
function route($route="index"){
    return $route;
}
//---------------------------------
//---------------------------------


class template{
    private $template;
    function __construct($template = "main"){
        $this->template = $template;
        ob_start();
    }
    public function render($arrConf=array()){
        $content      = ob_get_clean();//contenido dinamico
        $title        = (isset($arrConf["title"]))?  $arrConf["title"]: "Title";
        $description  = (isset($arrConf["description"]))? $arrConf["description"]: "Description";
        $path = dirname(__FILE__,2)."/resources/layouts/_".$this->template.".php";
        if(file_exists($path)){
            require_once $path;
        }else{
            die("No se encontró la pagina");
        }
    }
}
?>


