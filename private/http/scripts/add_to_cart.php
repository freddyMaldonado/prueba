<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 03:43 PM
 */
header('Content-Type: application/json');
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $validar["id"]          = array("etiqueta" => "Identificador"   , "required" => "1", "type"=> "number");
    $validar["category"]    = array("etiqueta" => "Categoria"       , "required" => "1", "type"=> "text");
    $validar["name"]        = array("etiqueta" => "Nombre Servicio" , "required" => "1", "type"=> "text");
    $validar["price"]       = array("etiqueta" => "Price" , "required" => "1", "type"=> "number");

    $validacion  = validador($validar,$_POST);

    if($validacion["success"] == "1"){
        $datos        = $_POST;
        $datos["qty"] = "1";
        if(!isset($_SESSION["cart"][$datos["id"]])){//si ya existe el elemento le agregamo
            $_SESSION["cart"][$datos["id"]] = $datos;// lo agregamos al carrito
            die(json_encode(array("success" => "1", "msg" => "El servicio: <b>".$datos["name"]."</b> se agregó correctamente","qty"=> count($_SESSION["cart"]))));
        }else{
            die(json_encode(array("success" => "0", "msg" => "El vehiculo  <b>".$datos["name"]."</b> ya se encuentra en el carrito" )));
        }
    }else{
        die(json_encode(array("success" => "0", "msg" =>  $validacion["errores"][0]["msg"])));
    }


}