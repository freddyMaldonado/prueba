<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 05:07 PM
 */
header('Content-Type: application/json');
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $validar["id"]          = array("etiqueta" => "Identificador"   , "required" => "1", "type"=> "number");
    $validacion  = validador($validar,$_POST);
    if($validacion["success"] == "1"){
        $id         = $_POST["id"];
        if(isset($_SESSION["cart"][$id])){//si ya existe el elemento le agregamo
            $name_service = $_SESSION["cart"][$id]["name"];
            unset($_SESSION["cart"][$id]);
            die(json_encode(array("success" => "1", "msg" => "El vehiculo : <b>".$name_service."</b> se eliminó correctamente","qty"=> count($_SESSION["cart"]))));
        }else{
            die(json_encode(array("success" => "0", "msg" => "No se encontró el elemento" )));
        }
    }else{
        die(json_encode(array("success" => "0", "msg" =>  $validacion["errores"][0]["msg"])));
    }


}