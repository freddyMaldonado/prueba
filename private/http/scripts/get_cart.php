<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 03:42 PM
 */

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(is_array($_SESSION["cart"]) && count($_SESSION["cart"])>0){
        $cart  = $_SESSION["cart"];
        ?>
        <div class="table-responsive">
            <table class="table table-striped tbl-cart">
                <thead>
                <tr>
                    <th>Sku</th>
                    <th>Vehiculo</th>
                    <th>Categoria</th>
                    <th>Imagen</th>
                    <th class="text-center">Qty</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(is_array($cart) && count($cart)>0){
                    foreach ($cart as $id => $item){
                        ?>
                        <tr class="is-row" data-price="<?php echo $item["price"]?>" >
                            <td><?php echo $item["id"]?></td>
                            <td><?php echo $item["name"]?></td>
                            <td><?php echo $item["category"]?></td>
                            <td><img class="img-thumbnail" src="<?php echo asset("images/carts/id_".$item["id"].".jpg")?>"></td>
                            <td>
                                <form class="frm-qty">
                                    <input name="id" value="<?php echo $item["id"]?>" type="hidden">
                                    <select class="form-control item-qty" name="qty">
                                        <?php
                                        for($_a=1; $_a<= 5; $_a++){
                                            $selected = ($_a == $item["qty"])?"selected": "";
                                            ?>
                                            <option value="<?php echo $_a?>" <?php echo $selected?>><?php echo $_a?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </form>

                            </td>
                            <td>$<?php echo $item["price"]?> MXN</td>
                            <td><button class="btn btn-outline-danger btn-delete-item" data-identificador="<?php echo $item["id"]?>"><i class="far fa-trash-alt"></i> Eliminar </button></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td colspan="5" class="text-right">Total:</td>
                    <td colspan="2" id="total-cart"><?php echo $total?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <script>
            $(".btn-delete-item").off("click").on("click",function () {
                var identificador = $(this).data("identificador");
                var request  = {
                    url:"delete_item_cart",
                    data:{id:identificador},
                    method:"POST",
                    dataType: "JSON"
                };
                main_async_request(request).then(
                    function (resp){
                        var classe = "danger";
                        if(resp['success'] === '1'){
                            classe= "success";
                            update_qty_cart(resp['qty']);
                            $("#cart-list").trigger("click");
                        }
                        show_notification(resp['msg'],classe);
                    }
                );
            });

            $(".item-qty").off("change").on("change",function () {
                var form = $(this).closest(".frm-qty");
                var request = {
                    url:"update_item_cart",
                    data: form.serializeArray(),
                    method:"POST",
                    dataType: "json"
                };
                main_async_request(request).then(
                    function (resp){
                        var classe = "danger";
                        if(resp['success'] === '1'){
                            classe= "success";
                            calculate_total_cart();
                        }
                        show_notification(resp['msg'],classe);
                    }
                );
            });
            $(function () {
               calculate_total_cart();
            });
        </script>
        <?php
    }else{
        ?>
        <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
            <strong>No se han agregado vehiculos al carrito de compras.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
    }

}