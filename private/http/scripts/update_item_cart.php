<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 21/07/2020
 * Time: 05:07 PM
 */
header('Content-Type: application/json');
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $validar["id"]          = array("etiqueta" => "Identificador"   , "required" => "1", "type"=> "number");
    $validar["qty"]         = array("etiqueta" => "Qty"   , "required" => "1", "type"=> "number");
    $validacion  = validador($validar,$_POST);
    if($validacion["success"] == "1"){
        $datos = $_POST;
        if(isset($_SESSION["cart"][$datos["id"]])){
            $_SESSION["cart"][$datos["id"]]["qty"] = $datos["qty"];
            die(json_encode(array("success" => "1", "msg" => "Se actualizó correctamente")));
        }else{
            die(json_encode(array("success" => "0", "msg" => "No se encontró el elemento" )));
        }
    }else{
        die(json_encode(array("success" => "0", "msg" =>  $validacion["errores"][0]["msg"])));
    }


}