/**
 * Created by Alfredo on 21/07/2020.
 */
function main_async_request(data){//centralizo todas las peticiones AJAX
    var url         = ("url" in data)? data['url']:"";
    var method      = ("method" in data)? data['method']: "POST";
    var data        = ("data" in data)? data['data']:{};
    var dataType    = ("dataType" in data)?data['dataType']:"";
    return new Promise(function(fn_success,fn_fail){
        $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: dataType
        }).done(function (res) {
            fn_success(res);
        }).fail(function (err) {
            fn_fail(err);
        })
    });
}
function update_qty_cart(qty_items){
    $("#qty-items").text(qty_items);
}
function get_cart_list(){
    var request = {url:"get_cart",method:"POST"};
    main_async_request(request).then(
        function (resp) {
            $("#main-modal").modal("show");
            $("#main-modal").find(".modal-title").html("<i class='fas fa-shopping-cart'></i> Carrito de compras");
            $("#main-modal").find(".modal-body").html(resp);
        }
    )

}
function show_notification(msj,classe){
    var template = '<div data-notify="container" class="col-xs-8 col-sm-4 alert alert-__class__ animated fadeInDown" role="alert" data-notify-position="top-right" style=" width:60%;display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 9999; top: 20px; right: 30px;"><button type="button" aria-hidden="true" class="close" data-notify="dismiss" data-dismiss="alert" style="position: absolute; right: 10px; top: 5px; z-index: 1033;">×</button><span data-notify="icon"></span> <span data-notify="title"></span> <span data-notify="message">__text__</span><a href="#" target="_blank" data-notify="url"></a></div>';
    template = template.replace("__class__",classe);
    template = template.replace("__text__",msj);
    $("#notifications").html(template);
    window.setTimeout(function () {//se cierra  a los 3 segundos
        $("#notifications").html("");
    },3000);
}
function notify(){
    $.notify({
        // options
        message: 'Hello World'
    },{
        // settings
        type: 'danger'
    });
}
function calculate_total_cart() {
    var total = 0;
    $(".is-row").each(function () {
        var cuantos = $(this).find(".item-qty").val();
        total += $(this).data("price")*cuantos;
    });
    var strTotal = "$"+String(total)+" MXN";
    $("#total-cart").html(strTotal);
}



